<?php
/**
 * Created by PhpStorm.
 * User: Hussein Ahmed
 * Date: 7/12/2018
 * Time: 12:53 PM
 */

//------------------------- Register Sidebars -----------------------

/**
 * Adding Widget Ready Areas or Sidebar
 * */
function dot_custom_sidebars() {

    $args = array(
        'id'            => 'custom_sidebar',
        'name'          => __( 'Custom Widget Area', 'text_domain' ),
        'description'   => __( 'A custom widget area', 'text_domain' ),
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
    );
    register_sidebar( $args );

}
add_action( 'widgets_init', 'dot_custom_sidebars' );

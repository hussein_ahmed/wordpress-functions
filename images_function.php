<?php
/**
 * Created by PhpStorm.
 * User: Hussein Ahmed
 * Date: 6/19/2018
 * Time: 12:38 PM
 */


#------------------------------- Image setting ---------------------------
/**
 * its Wordpress function use to create theme support feature image
 * add_image_size to create image size and call it by name
*/
function theme_setup()
{
    //Add featured image support
    add_theme_support('post-thumbnails');
    add_image_size('small-thumbnail', 200, 300, array('left', 'top'), true); //size of image used name at attach function
}

add_action('after_setup_theme', 'theme_setup');

//image size for custom filed support Pods Plugin
add_image_size('press', 1044, 470, true);


#------------------------------- Images ---------------------------
/**
 * GET Attachment image URL.
 * size var its take string value name of image size that add at your setting
 */
//attachment tumbnail image
function dot_attach_image_url($post_id, $size){
    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), $size)[0];
    return $image;
}

/**
 * GET image at custom filed.
 * name var its name of custom filed
 * count var if custom filed have multi image can use function into Array
 */
function dot_custom_filed_images($post_id, $name, $count = 0){
    $floor = get_post_meta($post_id, $name)[$count]['guid']; //count cause if post have multi image at same filed
    return $floor;
}

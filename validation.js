/**
 * front validation return error massage with input id
 * id param its from id
 * email param take true if your form has input type email
 * number param if form has input should have number only and give this input id = number
 * inputLength param object array take input id has object array take min and max input number
 * */

function dot_validation(id, email = null, number = null, inputLength = null) {
    var ele = document.forms[id].elements;
    var length = ele.length;
    var errors = [];

    $.each(ele, function (i, val) {

        if (--length <= 0) {
            return false;
        }

        var id = $(val).attr('id');
        var name = $(val).attr('name');
        var type = $(val).attr('type');
        var value = $("#" + id).val();

        if (value == '') {
            errors.push({message: name + " its required", id: id});
        }
        if (inputLength != null) {
            $.each(inputLength, function (inputId, length) {
                if (inputId == id && value != '') {
                    if (value.length < length['min'] || value.length > length['max']) {
                        errors.push({
                            message: name + " length should between " + length['min'] + " & " + length['max'],
                            id: id
                        });
                    }
                }

            });
        }

        if (email != null) {
            if (type == 'email' && value != '') {
                var atpos = value.indexOf("@");
                var dotpos = value.lastIndexOf(".");
                if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= value.length) {
                    errors.push({message: type + " Not a valid e-mail address!", id: id});
                }
            }
        }

        if (number != null) {
            if (id == 'number' && value != '') {
                if (isNaN(value) == true) {
                    errors.push({message: "invalid number only contain number!", id: id});
                }
            }
        }

    });

    return errors;
}


//------------------------------- input length array example ------------------------

/*
inputLength = {

    "number":{
        min:2,
        max:5,
    },
    "usernamesignup":{
        min:10,
        max:20
    }
};*/

<?php
/**
 * Created by PhpStorm.
 * User: Hussein Ahmed
 * Date: 6/18/2018
 * Time: 12:38 PM
 */

#-----------------------------Mail---------------------------------
/**
 * MAIL Setting.
 */

function Mail_setting()
{

    function wpse27856_set_content_type()
    {
        return "text/html";
    }

    add_filter('wp_mail_content_type', 'wpse27856_set_content_type');
}

#---------------------------Send Template by E-mail ------------------

/**
 * Installation composer require anthonybudd/WP_Mail
 * required WP_Mail.php
 * at WP_Mail class ->template choose template and pass data to use at template
 */

#---------------- Example ----------------------------
add_action("wp_ajax_careers", "careers_function");
add_action("wp_ajax_nopriv_careers", "careers_function");

function careers_function()
{

    require_once(ABSPATH . "/vendor/anthonybudd/wp_mail/src/WP_Mail.php");

    $data = $_POST['datastring']; //received data

    $send_email = WP_Mail::init()
        ->to('eng.husseinad@gmail.com')
        ->subject('careers!')
        ->template(get_template_directory() . '/emails/template.php', [
            'data' => $data
        ])
        ->send();

    header('Content-Type: application/json');
    die(json_encode($send_email));

}
<?php
/**
 * Created by PhpStorm.
 * User: Hussein Ahmed
 * Date: 6/18/2018
 * Time: 12:38 PM
 */

#-------------------------------- My Post ------------------------------------------
/**
 * GET Posts.
*/
function dot_posts($post_type, $order, $category = null, $pagination = null, $orderby = null)
{

    $args = array(
        'post_type'         => $post_type,
        'category'          => $category,
        'order'             => $order,
        'orderby'           => $orderby,
        'posts_per_page'    => $pagination,
    );

    $post = get_posts($args);

    return $post;
}

#------------------------------ Taxonomy Post ----------------------------------------

/**
 * GET Posts by taxonomy.
 * taxonomy var it's string value of taxonomy name
 * term_id var it's integer value take id of term
*/
function dot_archive_tax_post($post_type, $order, $taxonomy, $term_id){

    $project_args = array(
        'post_type' => $post_type,
        "order" => $order,
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomy,
                'terms' => $term_id
            )
        )
    );
    $projects = get_posts($project_args);

    return $projects;
}

#----------------------------- READ MORE ---------------------------------------

/**
 *
*/

function dot_read_more_link() {
    return '<a class="more-link" href="' . get_permalink() . '">Your Read More Link Text</a>';
}
add_filter( 'the_content_more_link', 'dot_read_more_link' );

#---------------------------- Excerpt ---------------------------------

/**
 * Change Excerpt Length
*/

function new_excerpt_length($length) {
return 100;
}
add_filter('excerpt_length', 'new_excerpt_length');

#-------------------------Upload Files ------------------------------------------

/**
 * Add Additional File Types to be Uploaded
*/

function dot_my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
    $mime_types['psd'] = 'image/vnd.adobe.photoshop'; //Adding photoshop files
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);
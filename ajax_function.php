<?php
/**
 * Created by PhpStorm.
 * User: Hussein Ahmed
 * Date: 6/18/2018
 * Time: 12:38 PM
 */

#---------------------------- Ajax --------------------------------------
/**
 * AJAX Ready.
 * function pass URL var to front to use at Ajax request
 * ajaxurl JavaScript var use at ajax request as URL
*/

function add_ajaxurl_cdata_to_front()
    {
?>
        <script type="text/javascript">
            ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>'; //call wordpress ajax file
        </script>
<?php
    }

add_action('wp_head', 'add_ajaxurl_cdata_to_front', 1);

#----------------------------- Ajax Example -------------------------------
/**
 * AJAX Example.
 * (wp_ajax_nopriv) handling AJAX requests from unauthenticated users (news_letter) action name
 * $wpdb Wordpress var to Add OR Edit at Database
 */

add_action("wp_ajax_news_letter", "news_letter_function");
add_action("wp_ajax_nopriv_news_letter", "news_letter_function");

function news_letter_function()
{
    global $wpdb; // Wordpress class to add or edit at DataBase

    $email = $_POST['email']; // received data from ajax request
    $arr = [];
    $index = 0;

    $date = new DateTime();
    $created_at = $date->getTimestamp();

    //Back-End validation
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'Invalid email format!';

    } else {

        //insert at wordpress database
        $table_name = $wpdb->prefix . "wysija_user";
        $test = $wpdb->insert($table_name, array('email' => $email, 'created_at' => $created_at, 'status' => 1));

        $sql = "INSERT INTO 'wp_wysija_user' ('email', 'created_at', 'status') VALUES ($email,  '0',  '1')";

        if ($test == false) {
            $errors[] = 'You are already subscribed!';
        }
    }

    if (!empty($errors)) {

        foreach ($errors as $key => $all) {
            $arr[$index]['error'] = $all;
        }

    } else {
        $arr[$index]['success'] = 'Thank You!';
        $arr[$index]['error'] = '';
    }


    header('Content-Type: application/json');
    $text = json_encode($email);
    die(json_encode($arr));     //return data to ajax request

}

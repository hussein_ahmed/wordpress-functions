<?php
/**
 * Created by PhpStorm.
 * User: Hussein Ahmed
 * Date: 6/24/2018
 * Time: 12:38 PM
 */

#---------------------------Security---------------------------------------
/**
 * Disable XML-RPC
 * XML-RPC is a method that allows third party apps to communicate with your WordPress site remotely.
 * This could cause security issues and can be exploited by hackers.
*/

add_filter('xmlrpc_enabled', '__return_false');

#----------------------------REGISTERATION-----------------------------------
/**
 * Registration system
 * $wpdb var to insert at Database
 * data received by Ajax
 * can add more information to user account
 */

add_action("wp_ajax_register", "dot_register_function");
add_action("wp_ajax_nopriv_register", "dot_register_function");

function dot_register_function()
{

    global $wpdb;

    $data = $_POST;
    $name = $data['usernamesignup'];
    $email = $data['emailsignup'];
    $password = $data['passwordsignup'];
    $confirm = $data['passwordsignup_confirm'];

    $website = "http://localhost/wordpress";
    $userdata = array(
        'user_login'    => $name,
        'user_email'    => $email,
        'user_url'      => $website,
        'user_pass'     => $password, // When creating an user, `user_pass` is expected.
        'role'          => 'Author'
    );

    $user_id = wp_insert_user($userdata);

    header('Content-Type: application/json');
    $text = json_encode($email);
    die(json_encode($user_id));
}

#--------------------------------LOGIN---------------------------------
/**
 * Login system
 * data received by Ajax
 * can logout by wp_logout() Wordpress function
 */

add_action("wp_ajax_login", "dot_login_function");
add_action("wp_ajax_nopriv_login", "dot_login_function");

function dot_login_function()
{

    $data = $_POST;

    $name = $data['username'];
    $password = $data['password'];

    $creds = array(
        'user_login' => $name,
        'user_password' => $password,
        'remember' => true
    );

    $user = wp_signon($creds, true);

    if (is_wp_error($user)) {
        echo $user->get_error_message();
    }

    var_dump($user);
}

#------------------------------Add Author profile fields-------------------------
/**
 *  Add extra fields to your author profiles
 *  This code will add Twitter and Facebook fields to user profiles.
 * You can now display these fields in your author template by (echo $curauth->twitter;)
 */

function dot_new_contactmethods( $contactmethods ) {
// Add Twitter
    $contactmethods['twitter'] = 'Twitter';
//add Facebook
    $contactmethods['facebook'] = 'Facebook';

    return $contactmethods;
}
add_filter('user_contactmethods','dot_new_contactmethods',10,1);

#------------------------------login Error-----------------------------------

/**
 *  Hide Login Errors
 * Add you own error
*/

function no_wordpress_errors(){
    return 'Something is wrong!';
}
add_filter( 'login_errors', 'no_wordpress_errors' );

#-----------------------------Post Author info ---------------------------------

/**
 * Add an Author Info Box in Post
 * with box css style
 */

function dot_author_info_box( $content ) {

    global $post;

// Detect if it is a single post with a post author
    if ( is_single() && isset( $post->post_author ) ) {

// Get author's display name
        $display_name = get_the_author_meta( 'display_name', $post->post_author );

// If display name is not available then use nickname as display name
        if ( empty( $display_name ) )
            $display_name = get_the_author_meta( 'nickname', $post->post_author );

// Get author's biographical information or description
        $user_description = get_the_author_meta( 'user_description', $post->post_author );

// Get author's website URL
        $user_website = get_the_author_meta('url', $post->post_author);

// Get link to the author archive page
        $user_posts = get_author_posts_url( get_the_author_meta( 'ID' , $post->post_author));

        if ( ! empty( $display_name ) )

            $author_details = '<p class="author_name">About ' . $display_name . '</p>';

        if ( ! empty( $user_description ) )
// Author avatar and bio

            $author_details .= '<p class="author_details">' . get_avatar( get_the_author_meta('user_email') , 90 ) . nl2br( $user_description ). '</p>';

        $author_details .= '<p class="author_links"><a href="'. $user_posts .'">View all posts by ' . $display_name . '</a>';

// Check if author has a website in their profile
        if ( ! empty( $user_website ) ) {

// Display author website link
            $author_details .= ' | <a href="' . $user_website .'" target="_blank" rel="nofollow">Website</a></p>';

        } else {
// if there is no author website then just close the paragraph
            $author_details .= '</p>';
        }

// Pass all this info to post content
        $content = $content . '<footer class="author_bio_section" >' . $author_details . '</footer>';
    }
    return $content;
}

// Add our function to the post content filter
add_action( 'the_content', 'dot_author_info_box' );

// Allow HTML in author bio section
remove_filter('pre_user_description', 'wp_filter_kses');

/*
.author_bio_section{
    background: none repeat scroll 0 0 #F5F5F5;
padding: 15px;
border: 1px solid #ccc;
}

.author_name{
    font-size:16px;
font-weight: bold;
}

.author_details img {
    border: 1px solid #D8D8D8;
border-radius: 50%;
float: left;
margin: 0 10px 10px 0;
}
This is how your */


